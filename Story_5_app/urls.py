from django.urls import path
from django.contrib import admin
from . import views

app_name = "Story_5_app" 

urlpatterns = [
	path ('admin/', admin.site.urls),
	path ('', views.homepage, name='homepage'),
	path ('createJadwal/', views.create_jadwal, name='createJadwal'),
	path ('delete/', views.delete, name="delete")
]