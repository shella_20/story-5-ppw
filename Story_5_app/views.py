from django.shortcuts import render, redirect
from . import forms
from .models import Jadwal
from django.utils import timezone
from .forms import JadwalForm
import datetime

# Create your views here.
def homepage(request):
	semua_jadwal = Jadwal.objects.all()
	return render(request, 'Homepage.html', {"jadwal": semua_jadwal})

def create_jadwal(request):
    form = JadwalForm()
    if request.method =="POST":
        form=JadwalForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('Story_5_app:createJadwal')
    return render(request, "Create Jadwal.html", {'form':form})

def jadwal(request):
    jadwals = Jadwal.objects.order_by("tanggal", "waktu")
    response = {
        'jadwal' : jadwals,
        'server_time' : datetime.datetime.now(),
    }
    return render(request, "Create Jadwal.html", response)

def delete(request):
    if request.method == "POST":
        id = request.POST['id']
        Jadwal.objects.get(id=id).delete()
    return redirect('Story_5_app:homepage')
